#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "customlibs/piles.h"

/* Macro définissant w(i,j) */
#define w(i,j) matrix[(i)][(j)]

/* Macro définissant min */
#define min(i,j) ((i) < (j) ? (i) : (j))

/* Booléen qui active le debuggage */
int verbose = 0;

/*-----------------------------------------------------------*/

/* Structure map, où sont stockées les informations obtenues, comme:
 * - Le tableau des di, D
 * - Le tableau des noeuds visités
 * - Le nombre de noeuds */

typedef struct {
    long *D;
    int *visited;
    int n;
} map;

/*-----------------------------------------------------------*/

/* @requires: un entier n
 * @assigns: un tableau d'entiers de taille n
 * @ensures: le tableau est défini, erreur sinon */

int* initialize_array_int(int n) {
    int *out;
    if ((out = malloc(n*sizeof(int))) == NULL) {
	fprintf(stderr, "Error: Couldn't allocate space for int array\n");
	exit(EXIT_FAILURE);
    }
    return out;
}

/*-----------------------------------------------------------*/

/* @requires: un entier n
 * @assigns: un tableau de longs de taille n
 * @ensures: le tableau est défini, erreur sinon */

long* initialize_array_long(int n) {
    long *out;
    if ((out = (long*) malloc(n*sizeof(long))) == NULL) {
	fprintf(stderr, "Error: Couldn't allocate space for long array\n");
	exit(EXIT_FAILURE);
    }
    return out;
}

/*-----------------------------------------------------------*/

/* @requires: la matrice d'entiers, le numero du noeud, la taille du tableau
 * @assigns: ajoute à la pile les voisins de node
 * @ensures: le nombre de voisins ajoutés est renvoyé */

int add_peers(int **matrix, stack* peers, int node, int n) {
    int added = 0, i;

    for (i = 0; i < n; ++i) {
	if (w(node, i) != -1) {
	    stack_push(peers, i);
	    added++;
	}
    }

    return added;
}

/*-----------------------------------------------------------*/

/* @requires: un pointeur vers l'objet map correspondant au problème;
 * @assigns: rien
 * @ensures: l'entier renvoyé correspond au noeud à visiter ensuite */

int choose(map *data){
    int k, min = -1;

    for (k = 0; k < data->n; ++k) {
	if (min != -1) {
	    if(verbose) printf("%ld - %ld\n", data->D[min], data->D[k]);
	} else {
	    if(verbose) printf("min not set - %ld (%d)\n", data->D[k], data->visited[k]);
	}
	if (min == -1 && !data->visited[k] && data->D[k] != -1) {
	    min = k;
	    if(verbose) printf("MIN CHANGED\n");
	}
	else if (!data->visited[k] && data->D[k] != -1 && data->D[k] < data->D[min]) {
	    min = k;
	    if(verbose) printf("MIN CHANGED\n");
	}
    }

    if (min == -1) {
	for (k = 0; k < data->n; ++k) {
	    if (!data->visited[k])
		min = k;
	}
    }

    if(verbose) printf("min: %d - %ld\n", min, data->D[k]);
    return min;
}

/*-----------------------------------------------------------*/

/* @requires: la matrice des entiers;
 *	      un pointeur vers l'objet map correspondant au problème;
 *	      le noeud à considérer;
 *	      le nombre de voisins de ce noeud
 * @assigns: modifie D (dans data) pour le mettre à jour en considérant les voisins de u
 * @ensures: di (D[i]) contient le chemin minimal touvé jusque maintenant par le programme */

void update_D(int **matrix, map *data, stack* peers, int u, int todo) {
    int v, status;
    if(verbose) printf("From %d:\n",u);

    while (todo-- > 0) {
	status = stack_pop(peers, &v);
	if(verbose) printf("v: %d\n", v);
	if (status && data->D[v] == -1) {
	    if(verbose) printf("D[%d] <- %ld + %d\n", v, data->D[u], w(u,v));
	    data->D[v] = data->D[u] + w(u,v);
	} else {
	    if(verbose) printf("D[%d] <- %ld (%ld : %ld + %d)\n", v, min(data->D[v], data->D[u] + w(u,v)), data->D[v], data->D[u], w(u,v));
	    data->D[v] = min(data->D[v], data->D[u] + w(u,v));
	}
    }
}

/*-----------------------------------------------------------*/

/* @requires: un pointeur vers l'objet map correspondant au problème;
 * @assigns: rien
 * @ensures: data est affiché sur stdout */

void print_data(map* data) {
    int i;
    if(verbose) printf("data: __________\n");
    for (i = 0; i < data->n; ++i)
	if(verbose) printf("%d : %ld (%d)\n", i, data->D[i], data->visited[i]);
    if(verbose) printf("EOD\n");
}

/*-----------------------------------------------------------*/

/* @requires: la matrice des entiers;
 *	      le noeud de départ;
 *	      le noeud d'arrivée;
 *	      un objet map qui sera effacé
 * @assigns: remplit data pour qu'il corresponde au problème
 * @ensures: dt (D[t]) est la plus courte distance séparant s et t */

int dijkstra(int **matrix, int s, int t, map *data) {
    int u, todo;
    stack peers = init_stack();

    memset(data->D, -1, data->n*sizeof(long));
    memset(data->visited, 0, data->n*sizeof(int));

    data->D[s] = 0;
    data->visited[s] = 1;

    u = s;

    while (1) {
	print_data(data);
	data->visited[u] = 1;

	if (u == t)
	    return data->D[t];

	todo = add_peers(matrix, &peers, u, data->n);
	update_D(matrix, data, &peers, u, todo);
	u = choose(data);
    }
}

/*-----------------------------------------------------------*/

/* @requires: la matrice des entiers;
 *	      un pointeur vers l'objet map correspondant au problème;
 *	      le noeud d'arrivée;
 *	      une pile vide (en global)
 * @assigns: le chemin dans la pile
 * @ensures: le chemin est affiché à l'écran */

void go_back(int **matrix, map *data, int t) {
    long dt;
    int v, current = t;
    dt = data->D[t];

    stack path = init_stack();
    stack_push(&path, t);

    while (dt > 0) {
	for (v = 0; v < data->n; ++v) {
	    if(verbose) printf("%d: %d - %ld (%ld/%ld)\n",v, w(v,current), dt - data->D[v], dt, data->D[v]);
	    if (w(v,current) != -1 && w(v,current) == dt - data->D[v]) {
		dt -= w(v,current);
		stack_push(&path, v);
		current = v;
		break;
	    }
	}
    }

    while (stack_pop(&path, &current))
	printf("%d\n", current+1);
}

/*-----------------------------------------------------------*/

int main() {
    int n, s, t;
    scanf("%d",&n);
    int ** matrix = (int**) malloc(n*sizeof(int*));
    int i,j;
	
    for(i = 0; i < n; i++) {
        int* line = (int*) malloc(n * sizeof(int));
        matrix[i] = line;
        for(j = 0; j < n; j++) {
	    scanf("%d", &line[j]);
	}
    }
	
    scanf("%d", &s);
    scanf("%d", &t);

    --s;
    --t;

    map graph_data;					/* On initialise l'objet map où seront stockées les informations */

    graph_data.D = initialize_array_long(n);		/* On crée le tableau dans graph_data */
    graph_data.visited = initialize_array_int(n);	/* On crée le tableau dans graph_data */
    graph_data.n = n;					/* graph_data.n devient le nombre de noeuds */

    dijkstra(matrix, s, t, &graph_data);		/* On remplit graph_data */

    if (graph_data.D[t] == -1) {			/* Si dt est infini (= -1) */
	printf("Not connected\n");			/* Le graphe n'est pas connecté */
    } else {						/* sinon */
	go_back(matrix, &graph_data, t);		/* On calcule et affiche le chemin */
    }

    free(graph_data.D);					/* On libère la mémoire */
    free(graph_data.visited);
    
    for(i = 0; i < n; i++)
        free(matrix[i]);
    free(matrix);

    return EXIT_SUCCESS;
}

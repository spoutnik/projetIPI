#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "customlibs/piles.h"

#define path(i, j) matrix[(i)][(j)]

/*------------------------------------------------------------*/

/* @requires: La matrice V, le nombre de noeuds
 * @assigns: 
 * @ensures: Le contenu de la matrice est affiché à l'écran */

void print_V(int **V, int n) {
    printf("\n");
    int i, j;
    for (i = 0; i < n; ++i) {
	for (j = 0; j < n; ++j) {
	    printf("%d ", V[i][j]);
	}
	printf("\n");
    }
}

/*-----------------------------------------------------------*/

/* @requires: La matrice V, un entier k à chercher, le nombre de noeuds
 * @assigns: 
 * @ensures: un retour nul si l'entier est présent dans la matrice V */

int not_in_V(int **V, int k, int n) {
    int i, j;

    for (i = 0; i < n; ++i) {
	for (j = 0; j < n; ++j) {
	    if (V[i][j] == k) {
		return 0;
	    }
	}
    }

    return 1;
}

/*-----------------------------------------------------------*/

/* @requires: Un tableau vi contenant des -1 avant la fin, un entier k
 * @assigns: k à la première case libre de vi
 * @ensures: */

void add_to_vi(int *vi, int k) {
    int i = 0;
    while (vi[i++] != -1);

    vi[i-1] = k;
}

/*-----------------------------------------------------------*/

/* @requires: La matrice d'entrée, la matrice V, un noeud node, la taille, la ligne à remplir, et le noeud destination
 * @assigns: À vi sont ajoutés les successeurs de node
 * @ensures: L'arrêt et le retour non nul si t à été ajouté */

int populate_vi(int **matrix, int **V, int node, int n, int line, int t) {
    int k;

    for (k = 0; k < n; ++k) {
	if (path(node,k) && not_in_V(V, k, n)) {
	    add_to_vi(V[line], k);
	    if (k == t) {
		return 1;
	    }
	}
    }
    return 0;
}

/*-----------------------------------------------------------*/

/* @requires: La matrice d'entrée, l'ensemble vi, un numéro de noeud valide, la taille
 * @assigns: rien
 * @ensures: l'entier renvoyé correspond à un prédécesseur de k */

int predecessor(int **matrix, int *vi, int k, int n) {
    int i;

    for (i = 0; i < n; ++i) {
	if (vi[i] != -1 && path(vi[i],k)) {
	    return vi[i];
	}
    }

    return -1;
}

/*-----------------------------------------------------------*/

/* @requires: La matrice d'entrée, la matrice V vide, la taille, et le noeud de départ/d'arrivée
 * @assigns: A chaque ligne de V l'ensemble vi
 * @ensures: si t est dans V, il est à la ligne retournée  */

int create_V(int **matrix, int **V, int n, int s, int t) {
    int line = 0, found = 0, k;

    /* s est l'unique membre de V0 */
    V[line][0] = s;

    /*Tant que on ne dépasse pas le nombre de lignes et que t n'a pas été trouvé */
    while (line++ < n - 1 && !found) {
	for (k = 0; k < (n-1) && !found && V[line-1][k] != -1; ++k) {
	    found = populate_vi(matrix, V, V[line-1][k], n, line, t);
	}
    }

    /* On renvoie la dernière ligne considérée */
    return line - 1;
}

/*-----------------------------------------------------------*/

/* @requires: La matrice d'entrée, la matrice V, le noeud d'arrivée, la taille, un pointeur vers une pile, la line où se trouve le noeud d'arrivée
 * @assigns: Retrace le chemin et le stocke dans la pile
 * @ensures: path contient le chemin à suivre */

void get_path(int **matrix, int **V, int t, int n, stack *path, int line) {
    int current = t;
    stack_push(path, current);

    while (line > 0) {
	current = predecessor(matrix, V[--line], current, n);
	stack_push(path, current);
    }
}

/*-----------------------------------------------------------*/

/* @requires: Un pointeur vers la pile contenant le chemin à suivre
 * @assigns: Vide la pile
 * @ensures: Une pile vide en sortie */

void output_path(stack *path) {
    int i;

    while (stack_pop(path, &i)) {
	printf("%d\n", i+1);
    }
}

/*-----------------------------------------------------------*/

int main(void) {
    int i,j;
    int n, s, t, line;
    scanf("%d",&n);
    int** matrix = (int**) malloc(n*sizeof(int*));

    for(i = 0; i < n; i++) {
        int* line = (int*) malloc(n * sizeof(int));
        matrix[i] = line;
        for(j = 0; j < n; j++) {
	    scanf("%d", &line[j]);
	}
    }
	
    scanf("%d", &s);
    scanf("%d", &t);
    s--;
    t--;
  
    /* Dans le cas où il n'y a pas de déplacements à faire */
    if (s == t) {
	printf("%d\n", s+1);
	return EXIT_SUCCESS;
    }

    /* Préparation de la matrice V, où la ième ligne correspond à Vi */
    int **V = malloc(n*sizeof(int*));
    for(i = 0; i < n; i++) {
        int* vi = (int*) malloc(n*sizeof(int));
	memset(vi, -1, n*sizeof(int));
	V[i] = vi;
    }

    /* Remplissage de la matrice V */
    line = create_V(matrix, V, n, s, t);

    if (not_in_V(V, t, n)) { /* Dans le cas où t ne se trouve pas dans V */
	printf("Not connected\n");
    } else {
	stack path = init_stack();		    /* création de la pile de sortie */
	get_path(matrix, V, t, n, &path, line);	    /* Remplissage de la pile de sortie */
	output_path(&path);			    /* Affichage du chemin */
    }

    /* La pile est vidée par output_path, pas de fuites mémoire */
    for(i = 0; i < n; i++) {
	free(matrix[i]);
	free(V[i]);
    }

    free(matrix);
    free(V);

    return EXIT_SUCCESS;
}

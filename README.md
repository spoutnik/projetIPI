# __Rapport de projet - IPI 2017-2018__

__Jean-Baptiste Skutnik - Groupe 4.2__

# *Exercice I*

## Choix de structures

### La matrice V

J'ai choisi d'utiliser une matrice pour représenter les différentes classes de noeuds, où la `i`e ligne repésente `vi`. Cette matrice est donc de dimensions `nx(n-1)`:

- Le graphe peut être un circuit, auquel cas il y aurait `n` `vi`;
- Le graphe peut être complet, auquel cas `card(v1) = n-1`.

La modélisation est peu économe en taille: on réserve l'espace pour stocker `n^2 - n` entiers et on en utilise `n` au plus: ce choix entraîne cependant une plus grande clarté dans la lecture de V demandée par la suite du programme; de plus la taille relativement faible des échantillons d'entrée le permet.

### La pile de sortie

Dans "piles.c", je définis une pile chainée. Chaque élément contient un entier et un pointeur vers l'élément suivant: cette structure est utilisée par la suite pour inverser l'ordre des noeuds du chemin.

```c
struct stack_el {
    int value;
    struct stack_el* next;
};

typedef struct stack_el* stack;
```

L'algorithme décrit dans le sujet entame la résolution du problème par le dernier `vi` où se trouve `t`: les noeuds à emprunter sont donc __décrits dans l'ordre inverse__. Ils sont alors stockés dans la pile par `get_path`: lors de la restitution, ils sont alors affichés par `output_path` dans l'ordre inverse à celui de leur insertion dans cette pile.

# *Exercice II*

## Choix de structures

### La `map`

Il fallait stocker les informations obtenues par l'algorithme de Dijsktra pour pouvoir les réutiliser ensuite. De plus, la taille variable devait être connue pour presque toutes les fonctions. Plutôt que de mettre tous ces paramètres en variables globales, ou de faire des fonctions à plus de 3 arguments chacunes, j'ai créé un objet `map`:

```c
typedef struct {
    long *D;		// tableau de distances (longs pour le test 7)
    int *visited;	// tableau de booléens, si le noeud a été visité
    int n;		// nombre de noeuds, donc taille de D et visited
} map;
```

En passant un pointeur vers cet objet, j'ai accès partout à toutes les informations calculées.

### La pile

Je réutilise la pile définie dans `piles.h`. Elle possède la même fonction que dans l'exercice 1, mais sert aussi à stocker les noeuds voisins à visiter lors de l'application de l'algorithme de Dijkstra.

### Les types

Je fis l'erreur de ne pas prêter attention à la taille des chemins. `map` contenait initialement deux tableaux d'`int`, ce qui avait pour effet de causer un `interger overflow` et de donner des valeurs négatives au test 7. Passer aux `long` à résolu ce problème.

# *Exercice III*

## Choix des structures

### Le graphe

Le graphe est représenté par des noeuds liés entre eux. Ils sont définis par:

```c
struct node {
    int distance, move, type;
    char visited, considered;
    struct node *up, *down, *left, *right, *link;
};

typedef struct node* tree;
```

Ils contiennent:

- `int distance`: Leur distance au point de départ;
- `int type`: Un entier codant leur type (mur, chemin, clé, etc ..);
- `int move`: Un entier codant l'action effectuée pour arriver sur cette case;
- `char visited`: Un booléen indiquant si la case à été visitée;
- `char considered`: Un booléen indiquant si la case va être visitée;
- `struct node* ...`: des pointeurs vers les noeuds atteignables à partir de celui ci.

Ils sont organisés dans une 'carte' du labyrinthe, appelée `map`.

### La pile à tout faire

Une pile fut elle aussi implémentée: elle consiste en une suite d'éléments chainés, de définition:

```c
struct stack_el {
    int x, y, type;
    tree node;
    struct stack_el* next;
};
```

Les éléments contenus dans cette pile correspondent à plusieurs cas de figure: l'algorithme ayant besoin de piles pour y stocker différents types d'objets, j'ai créé une pile pouvant tout contenir plutot que plusieurs piles contenant un type d'objet chacune. Il existe des fonctions `stack_push_generic` et `stack_pop_generic` génériques mais jamais utilisées directement: les 'wrappers' `statck_push_<type>` et `stack_pop_<type>` sont utilisées à leur place.

## Fonctionnement de l'algorithme

### Création de la carte

```c
void map_from_matrix(char** entree_du_programme, tree** carte_vide, int taille)
```

L'algorithme commence par générer la carte du labyrinthe, une matrice de noeuds liés entre eux. Pour cela, un premier __parcours en profondeur__ est effectué pour __créer un noeud à chaque case atteignable__; dans ce cas, la pile est utilisée pour stocker les coordonnées des cases à parcourir et transformer en noeud par la suite. Une fois toutes les cases atteignables transformées en noeuds, la fonction `map_link` va lier ces noeuds entre eux. La carte est alors prête à être utilisée.

### Parcours

```c
int map_parse(tree node_depart, int cible, int comportement)
```

On va alors parcourir la carte. `map_parse` va partir de la node en premier argument, et effectuer un __parcours en largeur__; il va __mettre à jour les noeuds visités, leur distance au noeud de départ et la dernière action effectuée pour y arriver__. Si une case du type passé en second argument est trouvée, le programme s'arrête et renvoie sa distance, sinon, il renvoie 0 lorsque il n'est plus possible de continuer.  
__Il peut adopter deux comportement différents__: si `IGNORE_DOORS` est passé en argument, les portes seront ignorées; elles seront empruntées si `USE_DOORS` est passé à la place.

Le graphe va alors être parcouru une première fois, __en ignorant les portes__; il peut alors se passer deux choses: la porte est touvée, et `map_parse` renvoie `1`, ou toutes les cases atteignables sont atteintes, et `0` est renvoyé.

* __Si la porte n'est pas trouvée ou si `distance` de la node de sortie est supérieure au `timer`__: il faut la clé. Le programme cherche les coordonnées de la clé, puis affiche le chemin effectué pour l'atteindre, généré par `map_parse`. La carte est ensuite remise à `0` et `map_parse` pour trouver la sortie, en empruntant les portes. Le chemin trouvé est affiché.

* __Sinon__: le chemin trouvé est affiché.

### Nettoyage

Toutes les structures utilisées sont effacées: les piles sont effacées après utilisation, la carte en fin de programme.

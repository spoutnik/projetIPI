#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define min(A,B) ((A) > (B) ? (B) : (A))

enum moves {NOT_FOUND = 0, UP, DOWN, LEFT, RIGHT, WARP};
enum type {ENTRY = -1, WALL, PATH, DOOR, KEY, TP, EXIT};
enum actions {IGNORE_DOORS = 0, USE_DOORS};


/*##############################################################
     _                 
    | |_ _ __ ___  ___ 
    | __| '__/ _ \/ _ \
    | |_| | |  __/  __/
     \__|_|  \___|\___|
##############################################################*/

struct node {
    int distance, move, type;
    char visited, considered;
    struct node *up, *down, *left, *right, *link;
};

typedef struct node* tree;

/*------------------------------------------------------------*/

/* @requires: Un type, et un symbole
 * @assigns: Une zone mémoire pour un noeud du graphe
 * @ensures: Le noeud est prêt à l'emploi: son contenu est initialisé */

tree tree_init(int type) {
    tree out = malloc(sizeof(struct node));

    out->visited = 0;
    out->considered = 0;
    out->distance = -1;
    out->move = NOT_FOUND;
    out->type = type;
    out->up = NULL;
    out->down = NULL;
    out->left = NULL;
    out->right = NULL;
    out->link = NULL;

    return out;
}

/*##############################################################
         _             _    
     ___| |_ __ _  ___| | __
    / __| __/ _` |/ __| |/ /
    \__ \ || (_| | (__|   < 
    |___/\__\__,_|\___|_|\_\
##############################################################*/

#define stack_init() NULL

struct stack_el {
    int x, y, type;
    tree node;
    struct stack_el* next;
};

typedef struct stack_el* stack;

/*------------------------------------------------------------*/

/* @requires: Une pile à modifier;
 *	      Une abcisse à donner à l'élément;
 *	      Une ordonnée à donner à l'élément;
 *	      Un type à donner à l'élément;
 *	      Un noeud à conserver.
 * @assigns: Un nouvel élément en tête de pile, contenant les infos
 * @ensures: La pile reste toujours valide (NULL si vide) */

int stack_push_generic(stack *todo, int x, int y, int type, tree node) {
    stack new_el;
    if ((new_el = malloc(sizeof(struct stack_el))) == NULL) {
	fprintf(stderr, "Failed to allocate memory\n");
	return 0;
    }

    new_el->x = x;
    new_el->y = y;
    new_el->type = type;
    new_el->node = node;

    if (*todo == NULL) {
	new_el->next = NULL;
	*todo = new_el;
    } else {
	new_el->next = *todo;
	*todo = new_el;
    }
    return 1;
}

/*------------------------------------------------------------*/

/* @requires: Différentes informations
 * @assigns: Des éléments à la pile sans avoir à rentrer des valeurs inutiles
 * @ensures: Une traduction compréhensible par stack_push */

int stack_push_coords(stack *todo, int x, int y, int type) {
    return stack_push_generic(todo, x, y, type, NULL);
}

int stack_push_move(stack *todo, int move) {
    return stack_push_generic(todo, 0, 0, move, NULL);
}

int stack_push_node(stack *todo, tree node) {
    return stack_push_generic(todo, 0, 0, 0, node);
}

/*------------------------------------------------------------*/

/* @requires: Une pile à modifier;
 *	      Un pointeur où stocker l'abcisse;
 *	      Un pointeur où stocker l'ordonée;
 *	      Un pointeur où stocker le type;
 *	      Un pointeur où stocker l'arbre
 * @assigns: Les infos du premier élément de la pile dans les pointeurs
 * @ensures: Pas de fuites mémoires, booléen indiquant si la pile est vide ou non */

int stack_pop_generic(stack* todo, int *x, int *y, int *type, tree *node) {
    if (*todo == NULL) {
	return 0;
    }

    *x = (*todo)->x;
    *y = (*todo)->y;
    *type = (*todo)->type;
    *node = (*todo)->node;

    stack tmp = *todo;
    *todo = (*todo)->next;

    free(tmp);
    return 1;
}

/*------------------------------------------------------------*/

/* @requires: Différentes informations
 * @assigns: Des informations demandées dans les pointeurs
 * @ensures: Des pointeurs inutiles pour éviter les fuites mémoires */

int stack_pop_coords(stack* todo, int *x, int *y, int *type) {
    tree garbage;
    return stack_pop_generic(todo, x, y, type, &garbage);
}

int stack_pop_move(stack* todo, int *move) {
    int garbage_int;
    tree garbage_tree;
    return stack_pop_generic(todo, &garbage_int, &garbage_int, move, &garbage_tree);
}

int stack_pop_node(stack* todo, tree* node) {
    int garbage_int;
    return stack_pop_generic(todo, &garbage_int, &garbage_int, &garbage_int, node);
}

/*##############################################################
     _ __ ___   __ _ _ __  
    | '_ ` _ \ / _` | '_ | 
    | | | | | | (_| | |_) |
    |_| |_| |_|\__,_| .__/ 
                    |_|
##############################################################*/

/* @requires: Une taille
 * @assigns: Une matrice d'arbres de taille nxn
 * @ensures: Pas d'erreurs d'allocation */

tree** map_init(int n) {
    tree** map;
    int i;

    if ((map = malloc(n*sizeof(tree*))) == NULL) {
	fprintf(stderr, "Error allocating memory\n");
	exit(EXIT_FAILURE);
    }
    for (i = 0; i < n; ++i) {
	if ((map[i] = malloc(n*sizeof(tree))) == NULL) {
	    fprintf(stderr, "Error allocating memory\n");
	    exit(EXIT_FAILURE);
	}
    }

    return map;
}

/*------------------------------------------------------------*/

/* @requires: Une matrice d'arbres, une taille
 * @assigns: Efface tous les arbres de la matrice
 * @ensures: La matrice est vidée */

void map_clear(tree **map, int n) {
    int i, j;

    for (i = 0; i < n; ++i) {
	for (j = 0; j < n; ++j) {
	    if (map[i][j] != NULL) {
		free(map[i][j]);
	    }
	}
    }
}

/*------------------------------------------------------------*/

/* @requires: Une matrice d'arbres, une taille
 * @assigns: Efface la matrice
 * @ensures: La matrice n'existe plus */

void map_free(tree **map, int n) {
    int i;

    for (i = 0; i < n; ++i)
	free(map[i]);
    free(map);
}

/*------------------------------------------------------------*/

/* @requires: Une matrice d'arbres, une taille
 * @assigns: rien
 * @ensures: La matrice est affichée à l'écran, utile avec | grep --color=auto -P "1|" pour voir le chemin empruntable */

void map_print(tree **tab, int size) {
    int i, j;
    for (i = 0; i < size; ++i) {
	for (j = 0; j < size; ++j) {
	    printf("%d ", tab[i][j] != NULL);
	}
	printf("\n");
    }
}

/*------------------------------------------------------------*/

/* @requires: L'entrée, une matrice d'arbres, une taille, des coordonnées
 * @assigns: Rien
 * @ensures: La case x y est traduite en type (de l'enum type) */

int valid(char matrix[1000][1002], tree **map, int n, int x, int y) {
    if (x < 0 || y < 0 || x > n-1 || y > n-1 || matrix[x][y] == 'X' || map[x][y] != NULL) {
	return WALL;
    }
    if (matrix[x][y] == '.') {
	return PATH;
    }
    if (matrix[x][y] == 'a') {
	return KEY;
    }
    if (matrix[x][y] == 'A') {
	return DOOR;
    }
    if (matrix[x][y] == 'S') {
	return EXIT;
    }
    return TP;
}

/*------------------------------------------------------------*/

/* @requires: L'entrée, une matrice d'arbres, une taille, des coordonnées, une pile à modifier
 * @assigns: Ajoute à la pile les voisins de la case
 * @ensures: La pile contient des cases valides uniquement*/

void add_neighbors(char matrix[1000][1002], tree **map, int n, int x, int y, stack *todo) {
    int type;

    if ((type = valid(matrix, map, n, x-1, y)) != WALL) {
	stack_push_coords(todo, x-1, y, type);
    }
    if ((type = valid(matrix, map, n, x+1, y)) != WALL) {
	stack_push_coords(todo, x+1, y, type);
    }
    if ((type = valid(matrix, map, n, x, y-1)) != WALL) {
	stack_push_coords(todo, x, y-1, type);
    }
    if ((type = valid(matrix, map, n, x, y+1)) != WALL) {
	stack_push_coords(todo, x, y+1, type);
    }
}

/*------------------------------------------------------------*/

/* @requires: Une matrice d'arbres, des coordonées, une taille
 * @assigns: Des pointeurs vers leurs voisins à chaque noeud
 * @ensures: Les neouds à conidérer sont liés entre eux */

void map_link(tree **map, int x, int y, int n) {
    if (x > 0 && map[x-1][y] != NULL) {
	map[x][y]->up = map[x-1][y];
	map[x-1][y]->down = map[x][y];
    }
    if (x < n-1 && map[x+1][y] != NULL) {
	map[x][y]->down = map[x+1][y];
	map[x+1][y]->up = map[x][y];
    }
    if (y > 0 && map[x][y-1] != NULL) {
	map[x][y]->left = map[x][y-1];
	map[x][y-1]->right = map[x][y];
    }
    if (y < n-1 && map[x][y+1] != NULL) {
	map[x][y]->right = map[x][y+1];
	map[x][y+1]->left = map[x][y];
    }
}

/*------------------------------------------------------------*/

/* @requires: L'entrée, une matrice d'arbres, une pile, des coordonées, une taille
 * @assigns: Ajoute à la pile le portail correspondant, et lie les deux portails sinon
 * @ensures: Les portails sont explorés et liés entre eux */

int stargate(char matrix[1000][1002], tree **map, stack *todo, int x, int y, int n) {
    int i, j;
    char symbol = matrix[x][y];

    for (i = 0; i < n; ++i) {
	for (j = 0; j < n; ++j) {
	    if (matrix[i][j] == symbol && !(i == x && j == y)) {
		if (map[i][j] == NULL) {			/* Si l'autre portail n'a pas été créé */
		    stack_push_coords(todo, i, j, TP);		/* L'ajouter à la pile à faire */
		    return 1;					/* => La fonction sera rappelée sur l'autre */
		} else {					/* Si il existe */
		    map[i][j]->link = map[x][y];		/* Lier les portails */
		    map[x][y]->link = map[i][j];
		}
	    }
	}
    }
    return 0;
}

/*------------------------------------------------------------*/

/* @requires: L'entrée, une carte vide, une taille
 * @assigns: Remplit la carte avec les fonctions précédentes
 * @ensures: La carte contient les noeuds pouvant être considérés */

void map_from_matrix(char matrix[1000][1002], tree **map, int n) {
    int x = 0, y = 0, type = ENTRY;
    stack todo = stack_init();
    map[0][0] = tree_init(ENTRY);
    add_neighbors(matrix, map, n, x, y, &todo);

    while (stack_pop_coords(&todo, &x, &y, &type)) {
	if (map[x][y] == NULL) {
	    map[x][y] = tree_init(type);
	    if (type == TP) {
		stargate(matrix, map, &todo, x, y, n);
	    }
	    map_link(map, x, y, n);
	    add_neighbors(matrix, map, n, x, y, &todo);
	}
    }
}

/*##############################################################
     _ __ ___   __ _ _ __      _ __   __ _ _ __ ___  ___
    | '_ ` _ \ / _` | '_ \    | '_ \ / _` | '__/ __|/ _ \
    | | | | | | (_| | |_) |   | |_) | (_| | |  \__ \  __/
    |_| |_| |_|\__,_| .__/____| .__/ \__,_|_|  |___/\___|
                    |_| |_____|_|
##############################################################*/

/* @requires: Un noeud;
 *	      Une pile à modifier;
 *	      Une distance;
 *	      Une action;
 *	      Une cible à rechercher;
 *	      Un booléen de comportement
 * @assigns: La distance de la node à considérer;
 *	     L'action effectuée pour y arriver;
 *	     Le fait que la node ait été considérée;
 *	     Renvoie un booléen si la cible est atteinte;
 * @ensures: La pile contient uniquement des nodes à regarder */

int consider_peer(tree node, stack* holder, int dist, int move, int target, int use_doors) {
    if (node != NULL && !node->visited) {

	if (node->distance == -1)
	    node->distance = dist + 1;
	else
	    node->distance = min(node->distance, dist+1);

	node->move = move;

	if (!use_doors && node->type == DOOR)
	    return 0;

	if (!node->considered) {
	    stack_push_node(holder, node);
	    node->considered = 1;
	}
	
	if (node->type == target)
	    return 1;
    }
    return 0;
}

/*------------------------------------------------------------*/

/* @requires: Un noeud, la pile à modifier, la cible, le booléen de comportement
 * @assigns: Renvoie 0 si aucun des mouvements n'atteint la cible
 * @ensures: Tous les mouvements possibles sont considérés */

int add_objectives(tree node, stack* holder, int target, int use_doors) {
    int has_target_been_reached = consider_peer(node->up, holder, node->distance, UP, target, use_doors)
	+ consider_peer(node->down, holder, node->distance, DOWN, target, use_doors) 
	+ consider_peer(node->left, holder, node->distance, LEFT, target, use_doors) 
	+ consider_peer(node->right, holder, node->distance, RIGHT, target, use_doors)
	+ consider_peer(node->link, holder, node->distance - 1, WARP, target, use_doors);   /* Warp ne prend pas de distance*/
    return has_target_been_reached;
}

/*------------------------------------------------------------*/

/* @requires: Le point de départ, la cible, le booléen de comportement
 * @assigns: Crée et utilise des piles pour stocker les noeuds à faire à chaque étape
 * @ensures: Tous les chemins possibles sont explorés. Si la cible est atteinte, 1 est renvoyé, 0 sinon. Les piles sont vidées en fin */

int map_parse(tree root, int target, int use_doors) {
    int done = 0;
    tree current;
    stack todo = stack_init(), todo_next = stack_init();

    stack_push_node(&todo, root);
    root->distance = 0;

    while (!done && todo != NULL) {
	while (!done && stack_pop_node(&todo, &current)) {
	    current->visited = 1;
	    done = add_objectives(current, &todo_next, target, use_doors);
	}

	if (todo_next == NULL)
	    break;

	todo = todo_next;
	todo_next = stack_init();
    }
    
    while (stack_pop_node(&todo, &current));
    while (stack_pop_node(&todo_next, &current));

    return done;
}

/*------------------------------------------------------------*/

/* @requires: La carte du labyrinthe, la taille
 * @assigns: Remet à 0 les infos des noeuds du graphe
 * @ensures: La carte est prête à l'emploi */

void map_reset(tree** map, int n) {
    int i, j;
    tree current;
    for (i = 0; i < n; ++i) {
	for (j = 0;  j < n; ++j) {
	    if ((current = map[i][j])) {
		current->considered = 0;
		current->visited = 0;
		current->move = -1;
		current->distance = 0;
	    }
	}
    }
}

/*------------------------------------------------------------*/

/* @requires: Une pile d'entiers
 * @assigns: Efface la pile en affichant son contenu
 * @ensures: La pile est vidée et son contenu est affiché */

void print_moves(stack* moves) {
    int move;

    while (stack_pop_move(moves, &move)) {
	switch (move) {
	    case UP:
		printf("HAUT\n");
		break;

	    case DOWN:
		printf("BAS\n");
		break;

	    case LEFT:
		printf("GAUCHE\n");
		break;

	    case RIGHT:
		printf("DROITE\n");
		break;

	    case WARP:
		printf("TP\n");
		break;
	}
    }
}

/*------------------------------------------------------------*/

/* @requires: Une node, le type de node de départ
 * @assigns: Remplit la pile en ratraçant le chemin de node à origin
 * @ensures: La pile contient un chemin effectué */

void get_path(tree node, int origin) {
    tree current = node;
    stack moves = stack_init();

    while (current->type != origin) {
	switch (current->move) {
	    case UP:
		current = current->down;
		stack_push_move(&moves, UP);
		break;

	    case DOWN:
		current = current->up;
		stack_push_move(&moves, DOWN);
		break;

	    case LEFT:
		current = current->right;
		stack_push_move(&moves, LEFT);
		break;

	    case RIGHT:
		current = current->left;
		stack_push_move(&moves, RIGHT);
		break;

	    case WARP:
		current = current->link;
		stack_push_move(&moves, WARP);
		break;
	}
    }
    print_moves(&moves);
}

/*------------------------------------------------------------*/

/* @requires: La carte, une taille
 * @assigns: Rien
 * @ensures: Le noeud contenant la clé est renvoyé */

tree get_key(tree** map, int n) {
    int x = -1, y, i, j;

    for (i = 0; i < n; ++i) {
	for (j = 0;  j < n; ++j) {
	    if (map[i][j] && map[i][j]->type == KEY) {
		x = i;
		y = j;
	    }
	}
    }

    return map[x][y];
}

/*##############################################################
                     _       
     _ __ ___   __ _(_)_ ___  
    | '_ ` _ \ / _` | | '_  |
    | | | | | | (_| | | | | |
    |_| |_| |_|\__,_|_|_| |_|
##############################################################*/

int main() {
    int n;
    char matrix[1000][1002] = { "" };
    int i;
    int timer;
	
    scanf("%d",&n);
	fgetc(stdin);
    
    for(i = 0; i < n; i++) {
        fgets(matrix[i], 1002, stdin);
    }    
    scanf("%d", &timer);
    fgetc(stdin);

/*------------------------------------------------------------*/

    int success;
    tree entry_node, exit_node, key_node;

    tree** map = map_init(n);				/* Créé la carte */

    map_from_matrix(matrix, map, n);			/* Remplire la carte, en générant un graphe */
    entry_node = map[0][0];
    exit_node = map[n-1][n-1];

    success = map_parse(entry_node, EXIT, IGNORE_DOORS); /* Aller de l'entrée à la sortie, sans utiliser les portes */

    if (!success || exit_node->distance > timer) {	/* Si la sortie n'a pas été trouvée */
	key_node = get_key(map, n);			/* Trouver la clé */

	get_path(key_node, ENTRY);			/* Afficher le chemin pour aller à la clé, calculé au parse précédent */

	map_reset(map, n);				/* Reset la map pour parser à nouveau */
	map_parse(key_node, EXIT, USE_DOORS);		/* Aller de la clé à la sortie, en utilisant les portes */
	get_path(exit_node, KEY);			/* Afficher le chemin */
    } else {
	get_path(exit_node, ENTRY);			/* Afficher le chemin */
    }

    map_clear(map, n);
    map_free(map, n);

    return EXIT_SUCCESS;
}

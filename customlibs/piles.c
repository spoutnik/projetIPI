/*--------------------------------------------------------------
// Définition des piles ////////////////////////////////////////
--------------------------------------------------------------*/
#include <stdlib.h>
#include <stdio.h>

#include "piles.h"

#define init_stack() NULL

struct stack_el {
    int value;
    struct stack_el* next;
};

/*-----------------------------------------------------------*/

/* @requires: un pointeur vers une pile
 * @assigns: 
 * @ensures: le contenu de la pile est affiché à l'écran */

void print_stack(stack* todo) {
    stack current = *todo;

    while (todo != NULL && current != NULL) {
	printf("-> %d ", current->value);
	current = current->next;
    }

    printf("-> End of stack.\n");
} 

/*-----------------------------------------------------------*/

/* @requires: un pointeur vers une pile, un entier
 * @assigns: un nouvel élément de pile est créé
 * @ensures: un élément ajouté à la pile */

int stack_push(stack *todo, int value) {
    stack new_el;
    if ((new_el = malloc(sizeof(struct stack_el))) == NULL) {
	fprintf(stderr, "Failed to allocate memory\n");
	return 0;
    }

    new_el->value = value;

    if (*todo == NULL) {
	new_el->next = NULL;
	*todo = new_el;
    } else {
	new_el->next = *todo;
	*todo = new_el;
    }
    return 1;
}

/*-----------------------------------------------------------*/

/* @requires: un pointeur vers une pile, un pointeur vers un entier
 * @assigns: libère la mémoire occupée par le premier élément et stocke son contenu dans le pointeur passé en paramètre
 * @ensures: une pile sans son premier élément */

int stack_pop(stack* todo, int *value) {
    if (*todo == NULL) {
	return 0;
    }

    *value = (*todo)->value;

    stack tmp = *todo;
    *todo = (*todo)->next;

    free(tmp);
    return 1;
}


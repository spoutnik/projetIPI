#ifndef PILES_H
#define PILES_H

#define init_stack() NULL

typedef struct stack_el* stack;

void print_stack(stack* todo);

int stack_push(stack *todo, int value);

int stack_pop(stack* todo, int *value);

#endif
